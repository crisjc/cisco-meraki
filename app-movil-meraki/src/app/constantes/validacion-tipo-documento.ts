import { Validators } from "@angular/forms";

export const VALIDACION_CEDULA = [Validators.required];
export const MENSAJES_VALIDACION_CEDULA = {
  required: 'El campo cédula es obligatorio',
  cedulaValida: 'El campo cédula no es válida'
};
