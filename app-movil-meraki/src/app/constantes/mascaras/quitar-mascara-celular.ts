export function quitarMascaraCelular(telefono: string): string | null {
    if (typeof telefono === 'number' || !telefono) {
      return telefono;
    } else {
      const quitarParentesisIzquierdo = telefono.replace('(', '');
      const quitarParentesisDerecho = quitarParentesisIzquierdo.replace(')', '');
      const quitarEspacio = quitarParentesisDerecho.replace(' ', '');
      const quitarGuion1 = quitarEspacio.replace('-', '');
      const quitarGuion = quitarGuion1.replace('-', '');
      const nuevoTelefono = quitarGuion;
      return nuevoTelefono;
    }
  }
  