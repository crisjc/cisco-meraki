export function quitarMascaraCedula(cedula: string): string {
    if (!cedula) {
      return cedula;
    } else {
      const cedulaNueva = cedula.replace('-', '');
      return cedulaNueva;
    }
  }
  