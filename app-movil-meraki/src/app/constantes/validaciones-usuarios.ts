import { Validators } from "@angular/forms";
import { SOLO_LETRAS_ESPACIOS } from "./patrones/solo-letras-espacios";

export const VALIDACION_NOMBRE_Y_APELLIDO = [
  Validators.required,
  Validators.minLength(3),
  Validators.maxLength(30),
  Validators.pattern(SOLO_LETRAS_ESPACIOS)
];
export const MENSAJES_VALIDACION_NOMBRE_Y_APELLIDO = {
  required: "El campo nombre y apellido es obligatorio",
  minlength: "El campo nombre y apellido debe tener mínimo 3 caracteres",
  maxlength: "El campo nombre y apellido no debe tener mas de 30 caracteres ",
  pattern: "El campo nombre y apellido debe tener solo letras"
};

export const VALIDACION_CORREO = [Validators.required, Validators.email];
export const MENSAJES_VALIDACION_CORREO = {
  required: "El campo correo es obligatorio",
  email: "El campo correo debe ser uno válido",
  minlength: "El campo correo debe tener mínimo 3 caracteres",
  maxlength: "El campo correo no debe tener mas de 15 caracteres "
};

export const VALIDACION_PROVINCIA = [Validators.required];
export const MENSAJES_VALIDACION_PROVINCIA = {
  required: "El campo provincia es obligatorio"
};

export const VALIDACION_SERVICIO = [Validators.required];
export const MENSAJES_VALIDACION_SERVICIO = {
  required: "El campo servicio es obligatorio"
};

export const VALIDACION_ASUNTO = [
  Validators.required,
  Validators.minLength(3),
  Validators.maxLength(30)
];
export const MENSAJES_VALIDACION_ASUNTO = {
  required: "El campo asunto es obligatorio",
  minlength: "El campo asunto debe tener mínimo 3 caracteres",
  maxlength: "El campo asunto no debe tener mas de 30 caracteres "
};

export const VALIDACION_MENSAJE = [
  Validators.required,
  Validators.minLength(3),
  Validators.maxLength(280)
];
export const MENSAJES_VALIDACION_MENSAJE = {
  required: "El campo mensaje es obligatorio",
  minlength: "El campo mensaje debe tener mínimo 3 caracteres",
  maxlength: "El campo mensaje no debe tener mas de 280 caracteres "
};

export const VALIDACION_CELULAR = [Validators.required];
export const MENSAJES_VALIDACION_CELULAR = {
  required: "El campo celular es obligatorio",
  celularValido: "El campo celular debe tener 10 dígitos."
};

export const VALIDACION_TELEFONO_CONVENCIONAL = [];
export const MENSAJES_VALIDACION_TELEFONO_CONVENCIONAL = {
  telefonoValido: "El campo teléfono debe tener 9 dígitos"
};
