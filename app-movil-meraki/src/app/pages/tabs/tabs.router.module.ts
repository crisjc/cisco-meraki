import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page";

const routes: Routes = [
  {
    path: "tabs",
    component: TabsPage,
    children: [
      {
        path: "home",
        children: [
          {
            path: "",
            loadChildren: "../home/home.module#HomePageModule"
          }
        ]
      },
      {
        path: "informacion",
        children: [
          {
            path: "",
            loadChildren:
              "../informacion/informacion.module#InformacionPageModule"
          }
        ]
      },
      {
        path: "mapa/:_id",
        children: [
          {
            path: "",
            loadChildren: "../mapa/mapa.module#MapaPageModule"
          }
        ]
      },
      {
        path: "mapa",
        children: [
          {
            path: "",
            loadChildren: "../mapa/mapa.module#MapaPageModule"
          }
        ]
      },
      {
        path: "contacto",
        children: [
          {
            path: "",
            loadChildren: "../contacto/contacto.module#ContactoPageModule"
          }
        ]
      },
      {
        path: "notificacion",
        children: [
          {
            path: "",
            loadChildren:
              "../notificacion/notificacion.module#NotificacionPageModule"
          }
        ]
      },
      {
        path: "",
        redirectTo: "/tabs/home",
        pathMatch: "full"
      }
    ]
  },
  {
    path: "",
    redirectTo: "/tabs/home",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
