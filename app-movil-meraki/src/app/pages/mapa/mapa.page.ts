import { ContactoService } from "./../contacto/contacto.service";
import { Component, OnInit } from "@angular/core";
import { Map, latLng, tileLayer, Layer, marker } from "leaflet";
import { ActivatedRoute } from "@angular/router";
import {} from "leaflet-routing-machine";
import { EventoService } from "../home/home.service";
import { MapaService } from "./mapa.service";
@Component({
  selector: "app-mapa",
  templateUrl: "mapa.page.html",
  styleUrls: ["mapa.page.scss"]
})
export class MapaPage implements OnInit {
  map: Map;
  usuarios = [];
  latitudUser: number;
  longitudUser: number;
  latitudEvento: number;
  longitudEvento: number;
  isParam: string;

  constructor(
    private readonly _usuarioService: ContactoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _eventoService: MapaService
  ) {
    console.log("sta en mapa");
  }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe(
      res => {
        this.isParam = res.get("_id");
        if (this.isParam == null) {
          this.ubicacionUsuario();
        } else {
          this.calcularRutasPuntos();
        }

        console.log("valor router links", res.get("_id"));
      },
      error => console.log("valor router link", error)
    );
    // this.ubicacionUsuario();
    //this.calcularRutasPuntos()
  }

  ubicacionUsuario() {
    this.cargarUbicacionUsuario();
    setTimeout(() => {
      this.map = new Map("map").setView(
        [this.latitudUser, this.longitudUser],
        16
      );

      tileLayer("https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png", {
        attribution:
          '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>',
        minZoom: 1,
        maxZoom: 19
      }).addTo(this.map);
      marker([this.latitudUser, this.longitudUser])
        .addTo(this.map)
        .bindPopup("Tu<br>Ubicacion.")
        .openPopup();
    }, 50);
  }

  cargarUbicacionUsuario() {
    const valorId: string = "5ce1046b9eba6129c23f581e"; 
    this._usuarioService.findOne(valorId).subscribe(
      usuario => {
        this.latitudUser = usuario.location.lat;
        this.longitudUser = usuario.location.lng;
      },
      error => {
        console.log(error);
      }
    );
  }
  calcularRutasPuntos() {
    this.cargarUbicacionUsuario();
    this.cargarUbicacionEvento();
    setTimeout(() => {
      this.map = new Map("map").setView(
        [this.latitudUser, this.longitudUser],
        16
      );

      tileLayer("https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png", {
        attribution:
          '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>',
        minZoom: 1,
        maxZoom: 19
      }).addTo(this.map);
      marker([this.latitudUser, this.longitudUser])
        .addTo(this.map)
        .bindPopup("Punto Salida")
        .openPopup();
      marker([this.latitudEvento, this.longitudEvento])
        .addTo(this.map)
        .bindPopup("Destino<br>Evento.")
        .openPopup();
    }, 50);
  }
  cargarUbicacionEvento() {
    const valorId: string = this.isParam;
    this._eventoService.findOne(valorId).subscribe(
      evento => {
        this.latitudEvento = evento.location.lat;
        this.longitudEvento = evento.location.lng;
      },
      error => {
        console.log(error);
      }
    );
  }

  // map: Map;
  /*  myIcon = icon({
    iconUrl: "../../assets/icon/hospital-icon.svg",
    iconSize: [38, 50],
    iconAnchor: [22, 50],
    popupAnchor: [-3, -36]
  });
  constructor() {}

  ngOnInit() {
    this.cargarMapa();
  }

  cargarMapa() {
    setTimeout(() => {
      this.map = new Map("map").setView([-0.174826, -78.488873], 10);
      tileLayer("https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png", {
        attribution:
          '<a href="https://wikimediafoundation.org/wiki/Maps_Terms_of_Use">Wikimedia</a>',
        minZoom: 1,
        maxZoom: 19
      }).addTo(this.map);
      this.map.locate({ setView: true, maxZoom: 15 }).on(
        "locationfound",
        ubicacion => {
          console.log(ubicacion);
          marker([-0.172138, -78.493057], {
            icon: this.myIcon
          })
            .addTo(this.map)
            .bindPopup(
              "Hola Mundo" +
                ` <a href="https://www.google.com/maps/dir/${
                  ubicacion["latitude"]
                },${ubicacion["longitude"]}/-0.172138,-78.493057">Iess</a>`
            );
        },
        50
      );
    });
  }

  localizar() {
    this.map.locate({ setView: true, maxZoom: 15 });
  } */
}
