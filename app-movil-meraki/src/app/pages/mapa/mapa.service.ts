import { PrincipalRestService } from "src/principal-rest.service";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { EventoInterface } from '../home/home.interface';
@Injectable({
  providedIn: "root"
})
export class MapaService extends PrincipalRestService<EventoInterface> {
  constructor(private readonly _http: HttpClient) {
    super(_http);
    this.url = environment.urlServer;
    this.port = environment.port;
    this.segmento = "evento";
  }

}
