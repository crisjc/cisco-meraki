import { EventoInterface } from "./home.interface";
import { Component } from "@angular/core";
import { MenuController } from "@ionic/angular";
import { Router, RouterEvent } from "@angular/router";
import { NetworkInterface } from "@ionic-native/network-interface/ngx";
import { Uid } from "@ionic-native/uid/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { EventoService } from "./home.service";
@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  eventos: EventoInterface[] = [];
  constructor(
    private readonly _eventoService: EventoService,
    private readonly _router: Router,
    private readonly _menu: MenuController,
    private networkInterface: NetworkInterface,
    private uid: Uid,
    private androidPermissions: AndroidPermissions
  ) {
    this.listarEventos();
    console.log(
      "datos red",
      this.networkInterface
        .getWiFiIPAddress()
        .then(address =>
          console.info(`IP: ${Object.keys(address)}, Subnet: ${address.subnet}`)
        )
        .catch(error => console.error(`Unable to get IP: ${error}`))
    );

    console.log("condifo mac", this.uid.MAC);
    console.log("condifo uuid", this.uid.UUID);
    console.log("condifo imsi", this.uid.IMSI);
    console.log("condifo imei", this.uid.IMEI);
    console.log("condifo iccid ", this.uid.ICCID);
    this.getImei();
  }
  openFirst() {
    this._menu.enable(true, "first");
    this._menu.open("first");
  }

  openEnd() {
    this._menu.open("end");
  }

  openCustom() {
    this._menu.enable(true, "custom");
    this._menu.open("custom");
  }

  async getImei() {
    const { hasPermission } = await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.READ_PHONE_STATE
    );

    if (!hasPermission) {
      const result = await this.androidPermissions.requestPermission(
        this.androidPermissions.PERMISSION.READ_PHONE_STATE
      );

      if (!result.hasPermission) {
        throw new Error("Permissions required");
      }

      // ok, a user gave us permission, we can get him identifiers after restart app
      return;
    }

    return this.uid.MAC;
  }

  listarEventos() {
    this._eventoService.findAll().subscribe(eventos => {
      console.log(eventos);
      this.eventos = eventos;
    });
  }
}
