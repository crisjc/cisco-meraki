import { PrincipalRestService } from "src/principal-rest.service";
import { Injectable } from "@angular/core";
import { EventoInterface } from "./home.interface";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable()
export class EventoService extends PrincipalRestService<EventoInterface> {
  constructor(private readonly _http: HttpClient) {
    super(_http);
    this.url = environment.urlServer;
    this.port = environment.port;
    this.segmento = "evento";
  }
}
