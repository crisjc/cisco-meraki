import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { HomePage } from "./home.page";
import { LoginComponent } from "src/app/components/login/login.component";
import { HomeRoutingModule } from "./home-routing.module";
import { EventoService } from "./home.service";

@NgModule({
  imports: [IonicModule, CommonModule, FormsModule, HomeRoutingModule],
  declarations: [HomePage, LoginComponent],
  providers: [EventoService],
})
export class HomePageModule {}
