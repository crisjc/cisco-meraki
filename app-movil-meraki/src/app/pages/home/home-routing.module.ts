import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route, Routes } from '@angular/router';
import { HomePage } from './home.page';



const rutas: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: 'virtuales',
      },
      {
        path: 'terminos',
      }

    ]
  }

];
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(rutas),
    CommonModule
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
