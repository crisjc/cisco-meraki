export interface LocationInterface   {
  lat: number;
  lng: number;
  unc: number;
  x: Array<number>;
  y: Array<number>;
}
export interface EventoInterface {
  _id?: string;
  nombre: string;
  descripcion: string;
  direccion: string;
  horaInicio: string;
  imagen: string;
  enlace: string;
  location: LocationInterface
}
