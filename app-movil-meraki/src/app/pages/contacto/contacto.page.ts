import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import {
  VALIDACION_NOMBRE_Y_APELLIDO,
  VALIDACION_CORREO,
  VALIDACION_ASUNTO,
  VALIDACION_MENSAJE,
  MENSAJES_VALIDACION_NOMBRE_Y_APELLIDO,
  MENSAJES_VALIDACION_CORREO,
  MENSAJES_VALIDACION_ASUNTO,
  MENSAJES_VALIDACION_MENSAJE,
  VALIDACION_TELEFONO_CONVENCIONAL,
  VALIDACION_CELULAR,
  MENSAJES_VALIDACION_CELULAR,
  MENSAJES_VALIDACION_TELEFONO_CONVENCIONAL,
  VALIDACION_PROVINCIA,
  VALIDACION_SERVICIO,
  MENSAJES_VALIDACION_PROVINCIA
} from "../../constantes/validaciones-usuarios";
import { debounceTime } from "rxjs/operators";
import { generarMensajesError } from "../../funciones/generar-mensajes-error";
import { FormGroup, FormBuilder } from "@angular/forms";
import { environment } from "src/environments/environment";
import { PrincipalRestService } from "src/principal-rest.service";
import { ContactoInterface } from "./contacto.interface";
import { HttpClient } from "@angular/common/http";
import { ContactoService } from "./contacto.service";
@Component({
  selector: "app-contacto",
  templateUrl: "./contacto.page.html",
  styleUrls: ["./contacto.page.scss"]
})
export class ContactoPage implements OnInit {
  constructor(private contactoService: ContactoService) {}

  usuarios: Array<any>;

  ngOnInit() {
    this.obtenerUsuarios();
  }

  obtenerUsuarios() {
    console.log(this.contactoService.findAll());
    this.contactoService.findAll().subscribe(usuarios => {
      this.usuarios = usuarios;
      console.log(this.usuarios);
    });
  }
}

/*   datosFormularioContacto;
  habilitarBotonEnviar: boolean = true;

  formularioContacto: FormGroup;
  mensajesErrores = {
    nombreYApellido: [],
    telefono: [],
    celular: [],
    correoElectronico: [],
    provincia: [],
    asunto: [],
    menssaje: []
  };
  provincias = [];
  provinciasQuemadas = [
    "Azuay",
    "Bolívar",
    "Cañar",
    "Carchi",
    "Chimborazo",
    "Cotopaxi",
    "El Oro",
    "Esmeraldas",
    "Galápagos",
    "Guayas",
    "Imbabura",
    "Loja",
    "Los Ríos",
    "Manabí",
    "Morona Santiago",
    "Napo",
    "Orellana",
    "Pastaza",
    "Pichincha",
    "Santa Elena",
    "Santo Domingo de los Tsáchilas",
    "Sucumbíos",
    "Tungurahua",
    "Zamora Chinchipe"
  ];
  servicios = [];
  serviciosQuemados = [
    " Afiliación, Empleadores y Mora Patronal",
    " Cesantías",
    " Fondos de Reserva",
    " Préstamos Hipotecarios - BIESS",
    " Préstamos Quirografarios - BIESS",
    " Seguro Social Campesino",
    " Seguro de Pensiones",
    " Seguro de Riesgos del Trabajo",
    " Seguro de Salud",
    " Servicio de Afiliación Voluntaria",
    " Varios/Otros"
  ];
  @Output() contactoValido: EventEmitter<any | boolean> = new EventEmitter();
  numeroCedula: any;

  constructor(private readonly _formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.inicializarFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
    this.cargarInformacion();
  }

  cargarInformacion() {
    this.cargarProvincias();
    this.cargarServicios();
  }

  cargarProvincias() {
    this.provincias = this.provinciasQuemadas;
  }

  cargarServicios() {
    this.servicios = this.serviciosQuemados;
  }

  inicializarFormulario() {
    console.log("ola");
    this.formularioContacto = this._formBuilder.group({
      nombreYApellido: ["", VALIDACION_NOMBRE_Y_APELLIDO],
      telefono: ["", VALIDACION_TELEFONO_CONVENCIONAL],
      celular: ["", VALIDACION_CELULAR],
      correoElectronico: ["", VALIDACION_CORREO],
      provincia: ["", VALIDACION_PROVINCIA],
      servicio: ["", VALIDACION_SERVICIO],
      asunto: ["", VALIDACION_ASUNTO],
      mensaje: ["", VALIDACION_MENSAJE]
    });
  }

  private verificarCamposFormulario() {
    this.verificarCampoFormControl(
      "nombreYApellido",
      MENSAJES_VALIDACION_NOMBRE_Y_APELLIDO
    );
    this.verificarCampoFormControl("celular", MENSAJES_VALIDACION_CELULAR);
    this.verificarCampoFormControl(
      "telefono",
      MENSAJES_VALIDACION_TELEFONO_CONVENCIONAL
    );
    this.verificarCampoFormControl(
      "correoElectronico",
      MENSAJES_VALIDACION_CORREO
    );
    this.verificarCampoFormControl("provincia", MENSAJES_VALIDACION_PROVINCIA);
    this.verificarCampoFormControl("asunto", MENSAJES_VALIDACION_ASUNTO);
    this.verificarCampoFormControl("mensaje", MENSAJES_VALIDACION_MENSAJE);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioContacto.get(campo);
    campoFormControl.valueChanges.pipe(debounceTime(500)).subscribe(valor => {
      // console.log(valor, campo)
      this.mensajesErrores[campo] = generarMensajesError(
        campoFormControl,
        this.mensajesErrores[campo],
        mensajeValidacion
      );
    });
  }

  seteoNumeroDeCI(evento) {
    console.log("ci", evento);
    if (evento) {
      this.numeroCedula = undefined;
      this.inicializarFormulario();
      setTimeout(() => {
        this.numeroCedula = evento;
        this.verificarFormulario();
      }, 100);
    } else {
      this.numeroCedula = undefined;
    }
  }

  private verificarFormulario() {
    const formularioFormGroup = this.formularioContacto;
    formularioFormGroup.valueChanges.subscribe(formulario => {
      console.log("formulario", formulario);
      const contactoValido = formularioFormGroup.valid;
      console.log("valido", contactoValido, this.numeroCedula);
      // const numeroCedulaExiste = this.numeroCedula
      if (contactoValido) {
        const contactoAEnviar = {
          cedula: this.numeroCedula,
          contacto: formulario
        };
        this.contactoValido.emit(contactoAEnviar);
      } else {
        this.contactoValido.emit(false);
      }
    });
  }

  contactoRecibido(evento) {
    console.log("EVENTO", evento);
    if (evento) {
      console.log("entre", evento);
      this.datosFormularioContacto = evento;
      this.habilitarBotonEnviar = false;
    } else {
      console.log("no entre", evento);
      this.habilitarBotonEnviar = true;
    }
  }

  enviarContacto() {
    console.log("evento", this.datosFormularioContacto);
  } */
