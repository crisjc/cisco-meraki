export interface LocationInterface {
  lat: number;
  lng: number;
  unc: number;
  x: Array<number>;
  y: Array<number>;
}

export interface ContactoInterface {
  _id?: string;
  nombre: string;
  correo: string;
  passsword: string;
  rol: string;
  ip?: string;
  mac?: string;
  //eventos?: Array<EventoInterface>;
  // notificaciones?: Array<NotificacionInterface>;
  location?: LocationInterface;
}
