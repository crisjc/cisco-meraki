import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PrincipalRestService } from "src/principal-rest.service";
import { ContactoInterface } from "./contacto.interface";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class ContactoService extends PrincipalRestService<ContactoInterface> {
  constructor(private readonly _http: HttpClient) {
    super(_http);
    this.url = environment.urlServer;
    this.port = environment.port;
    this.segmento = "usuario";
  }
}
