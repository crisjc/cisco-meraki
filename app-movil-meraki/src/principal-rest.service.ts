import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
export abstract class PrincipalRestService<Entidad> {
  public url: string = "";
  public port: string = "";
  public segmento: string = "";
  public metodoFindOne: string = "/";
  public metodoFindAll: string = "";
  public metodoCreate: string = "";
  public metodoUpdateOne: string = "/";
  public metodoDelete: string = "/";
  protected cabecerasGenerales: { headers: HttpHeaders };

  // tslint:disable-next-line:variable-name
  constructor(private readonly _httpClient: HttpClient) {}

  public establecerCabecerasGenerales(cabeceras: {
    headers: HttpHeaders;
  }): void {
    if (cabeceras) {
      // tslint:disable-next-line:no-object-literal-type-assertion
      this.cabecerasGenerales.headers = {
        ...this.cabecerasGenerales.headers,
        ...cabeceras.headers
      } as HttpHeaders;
    }
  }

  public findOne(
    id: number | string,
    cabeceras?: { headers: HttpHeaders }
  ): Observable<Entidad> {
    let cabecerasDePeticion;

    if (cabeceras) {
      cabecerasDePeticion = JSON.parse(JSON.stringify(this.cabecerasGenerales));
      cabecerasDePeticion.headers = {
        ...cabecerasDePeticion.headers,
        ...cabeceras.headers
      };
    }

    return this._httpClient
      .get(
        `${this.url}:${this.port}/` +
          this.segmento +
          this.metodoFindOne +
          `${id}`,
        cabecerasDePeticion
      )
      .pipe(
        // tslint:disable-next-line:no-angle-bracket-type-assertion
        map((r: any) => {
          const respuesta: Entidad = r;
          return respuesta;
        })
      );
  }

  public findAll(
    criterioBusqueda = "",
    cabeceras?: { headers: HttpHeaders }
  ): Observable<Entidad[]> {
    let cabecerasDePeticion;

    if (cabeceras) {
      cabecerasDePeticion = JSON.parse(JSON.stringify(this.cabecerasGenerales));
      cabecerasDePeticion.headers = {
        ...cabecerasDePeticion.headers,
        ...cabeceras.headers
      };
    }
    return this._httpClient
      .get(
        `${this.url}:${this.port}/` +
          this.segmento +
          this.metodoFindAll +
          `?${criterioBusqueda}`,
        cabecerasDePeticion
      )
      .pipe(
        // tslint:disable-next-line:no-angle-bracket-type-assertion
        map((r: any) => {
          const respuesta: Entidad[] = r;
          return respuesta;
        })
      );
  }

  public create(
    registro: any,
    cabeceras?: { headers: HttpHeaders }
  ): Observable<Entidad> {
    let cabecerasDePeticion;

    if (cabeceras) {
      cabecerasDePeticion = JSON.parse(JSON.stringify(this.cabecerasGenerales));
      cabecerasDePeticion.headers = {
        ...cabecerasDePeticion.headers,
        ...cabeceras.headers
      };
    }
    return this._httpClient
      .post(
        `${this.url}:${this.port}/` + this.segmento + `${this.metodoCreate}`,
        registro,
        cabecerasDePeticion
      )
      .pipe(
        // tslint:disable-next-line:no-angle-bracket-type-assertion
        map((r: any) => {
          const respuesta: Entidad = r;
          return respuesta;
        })
      );
  }

  public updateOne(
    id: number | string,
    actualizacion: any,
    cabeceras?: { headers: HttpHeaders }
  ): Observable<Entidad> {
    let cabecerasDePeticion;

    if (cabeceras) {
      cabecerasDePeticion = JSON.parse(JSON.stringify(this.cabecerasGenerales));
      cabecerasDePeticion.headers = {
        ...cabecerasDePeticion.headers,
        ...cabeceras.headers
      };
    }
    return this._httpClient
      .put(
        `${this.url}:${this.port}/` +
          this.segmento +
          `${this.metodoUpdateOne}` +
          `${id}`,
        actualizacion,
        cabecerasDePeticion
      )
      .pipe(
        // tslint:disable-next-line:no-angle-bracket-type-assertion
        map((r: any) => {
          const respuesta: Entidad = r;
          return respuesta;
        })
      );
  }

  public deleteOne(
    id: number | string,
    cabeceras?: { headers: HttpHeaders }
  ): Observable<{ mensaje: string }> {
    let cabecerasDePeticion;

    if (cabeceras) {
      cabecerasDePeticion = JSON.parse(JSON.stringify(this.cabecerasGenerales));
      cabecerasDePeticion.headers = {
        ...cabecerasDePeticion.headers,
        ...cabeceras.headers
      };
    }
    return this._httpClient
      .delete(
        `${this.url}:${this.port}/` +
          this.segmento +
          `${this.metodoDelete}` +
          `${id}`,
        cabecerasDePeticion
      )
      .pipe(
        // tslint:disable-next-line:no-angle-bracket-type-assertion
        map((r: any) => {
          const respuesta: { mensaje: string } = r;
          return respuesta;
        })
      );
  }
}
