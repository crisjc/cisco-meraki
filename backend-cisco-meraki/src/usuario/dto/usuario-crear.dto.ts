import { IsNotEmpty, IsEmail, Matches, IsString } from 'class-validator';
import { LocationInterface } from 'scaneo/body.interface';
export class UsuarioCrearDto {
  @IsNotEmpty()
  @IsEmail()
  correo: string;

  @IsNotEmpty()
  @IsString()
  //@Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/)
  password: string;

  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  rol: string;

  @IsString()
  ip: string;

  @IsString()
  mac: string;

  location: LocationInterface;


}
