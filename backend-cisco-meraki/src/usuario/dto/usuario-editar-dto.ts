import {
  IsNotEmpty,
  IsEmail,
  Matches,
  IsString,
  IsOptional,
} from 'class-validator';
import { LocationInterface } from 'scaneo/body.interface';
export class UsuarioEditarDto {
  @IsNotEmpty()
  @IsOptional()
  @IsEmail()
  correo?: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  nombre?: string;

  @IsNotEmpty()
  @IsString()
  @IsOptional()
  rol: string;

  @IsString()
  ip: string;

  @IsString()
  mac: string;

  location: LocationInterface;


  
}
