import {
  Controller,
  Post,
  Param,
  Body,
  BadRequestException,
} from '@nestjs/common';
import { UsuarioLoginDto } from './dto/usuario-login-dto';
import { validate } from 'class-validator';
import { UsuarioService } from './usuario.service';
import { generarUsuarioLogin } from './funciones/generar-usuario-loguin';
import md5 from 'md5';

@Controller('autenticacion')
export class AutenticacionController {
  constructor(private readonly _usuarioService: UsuarioService) {}

  @Post('login')
  async autentificar(@Body('usuario') usuario, @Body('password') password) {
    const usuarioALoguearse = generarUsuarioLogin(usuario, password);
    const arregloErrores = await validate(usuarioALoguearse);
    const existenErrores = arregloErrores.length > 0;
    console.log(arregloErrores);
    if (existenErrores) {
      console.error('errores: logueando al usuario', arregloErrores);
      throw new BadRequestException('Parametros inocrrectos');
    } else {
      const usuarioEncontrado = await this._usuarioService.findEmail(
        usuarioALoguearse.usuario,
      );
      if (usuarioEncontrado) {
        const esPasswordCorrecto =
          usuarioEncontrado.password == usuarioALoguearse.password;
        if (esPasswordCorrecto) {
          return usuarioEncontrado;
        } else {
          console.error(
            'intento fallido:  paassword incorrecto',
            usuarioALoguearse.password,
          );
          throw new BadRequestException('error loguin');
        }
      } else {
        console.error('intento fallido:  no existe usuario', usuarioALoguearse);
        throw new BadRequestException('error loguin');
      }
    }
  }
}
