import * as mongoose from 'mongoose';
import { eventoSchema } from 'evento/evento.schema';
import { locationSchema } from 'scaneo/scaneo.shema';

export const notificacionSchema = new mongoose.Schema({
  mensaje: String,
  fecha: { type: Date, default: Date.now() },
  isleido: { type: Boolean, default: false },
});

export const usuarioSchema = new mongoose.Schema({
  nombre: String,
  correo: String,
  password: String,
  rol: String,
  ip: String,
  mac: String,
  eventos: [eventoSchema],
  notificaciones: [notificacionSchema],
  location: locationSchema,
});
