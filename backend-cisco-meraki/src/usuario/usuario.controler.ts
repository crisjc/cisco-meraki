import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
  BadRequestException,
  Query,
  HttpStatus,
  Res,
  NotFoundException,
} from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { crearUsuario } from './funciones/crear-usuario';
import { validate } from 'class-validator';
import { UsuarioEditarDto } from './dto/usuario-editar-dto';
import { editarUsuario } from './funciones/editarUsuario';
import { UsuarioInterface } from './usuario.interface';
import { UsuarioCrearDto } from './dto/usuario-crear.dto';

@Controller('usuario')
export class UsuarioController {
  constructor(private readonly _usuarioService: UsuarioService) {}

  // metodo obtener todos
  @Get('/')
  async obtenerTodosUsuarios(@Res() res) {
    const usuarios = await this._usuarioService.obtenerUsuarios();
    return res.status(HttpStatus.OK).json(usuarios);
  }

  // metodo obtener uno
  @Get('/:id')
  async obtenerUsuario(@Res() res, @Param('id') id) {
    const usuario = await this._usuarioService.obtenerUsuario(id);
    if (!usuario) throw new NotFoundException('Evento no existe');
    return res.status(HttpStatus.OK).json(usuario);
  }

  //crear un usuario
  @Post('/crear')
  async crearUsuario(@Res() res, @Body() crearUsuario: UsuarioCrearDto) {
    const usuario = await this._usuarioService.crearusuario(crearUsuario);
    return res.status(HttpStatus.OK).json({
      message: 'usuario creado',
      usuario,
    });
  }

  // actualizar usuario  /actualizar?id=5c9d45e705ea4843c8d0e8f7
  @Put('/actualizar')
  async actualizarUsuario(
    @Res() res,
    @Body() usuarioEditarDto: UsuarioEditarDto,
    @Query('id') id,
  ) {
    const actualizarUsuario = await this._usuarioService.actulizarUsuario(
      id,
      usuarioEditarDto,
    );
    if (!actualizarUsuario) throw new NotFoundException('Este usuario no existe');
    return res.status(HttpStatus.OK).json({
      message: 'Usuario actualizado correctamente',
      actualizarUsuario,
    });
  }

  // eliminar el usuario /eliminar?id=5c9d45e705ea4843c8d0e8f7
  @Delete('/eliminar')
  async eliminarUsuario(@Res() res, @Query('id') id) {
    const eliminarUsuario = await this._usuarioService.eliminarUsuario(id);
    if (!eliminarUsuario) throw new NotFoundException('Usuario no existe');
    return res.status(HttpStatus.OK).json({
      message: 'Usuario eliminado correctamente',
      eliminarUsuario,
    }); 
  }

}
