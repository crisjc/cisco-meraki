import { ScaneoModule } from './../scaneo/scaneo.module';
import { Module } from '@nestjs/common';
import { UsuarioController } from './usuario.controler';
import { UsuarioService } from './usuario.service';
import { usuarioSchema } from './usuario.schema';
import { AutenticacionController } from './autenticacion.controller';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  controllers: [UsuarioController, AutenticacionController],
  imports: [
    MongooseModule.forFeature([{ name: 'Usuario', schema: usuarioSchema }]),
    ScaneoModule,
  ],
  providers: [UsuarioService],
  exports: [UsuarioService],
})
export class UsuarioModule {}
