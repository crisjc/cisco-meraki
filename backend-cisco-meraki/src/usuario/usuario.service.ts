import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UsuarioInterface } from './usuario.interface';
import { UsuarioCrearDto } from './dto/usuario-crear.dto';
import { UsuarioEditarDto } from './dto/usuario-editar-dto';

import randomize from 'randomatic';
import { ScaneoService } from 'scaneo/scaneo.service';
import { from } from 'rxjs';

@Injectable()
export class UsuarioService {
  actualizarUsuario;
  scaneoIp: string;
  scanepMac: string;
  constructor(
    @InjectModel('Usuario')
    private readonly _usuarioModel: Model<UsuarioInterface>,
    private readonly _scaneoService: ScaneoService,
  ) {}

  //obtener todos los usuario
  async obtenerUsuarios(): Promise<UsuarioInterface[]> {
    const usuarios = await this._usuarioModel.find();
    return usuarios;
  }

  // Obtener un usuario
  async obtenerUsuario(usuarioId: string): Promise<UsuarioInterface> {
    const usuario = await this._usuarioModel.findById(usuarioId);
    return usuario;
  }

  // crear un usuario
  async crearusuario(
    crearUsuarioDto: UsuarioCrearDto,
  ): Promise<UsuarioInterface> {
    const nuevoUsuario = new this._usuarioModel(crearUsuarioDto);
    return nuevoUsuario.save();
  }

  // actulizar un usuario
  async actulizarUsuario(
    usuarioId: string,
    usuarioEditarDto: UsuarioEditarDto,
  ): Promise<UsuarioInterface> {
    from(this._scaneoService.obtenerUltimoEscaneo()).subscribe(
      ultimoScaneo => {
        if (ultimoScaneo) {
          ultimoScaneo.data.observations.forEach(observacion => {
            if (
              observacion.ipv4 === usuarioEditarDto.ip ||
              observacion.clientMac === usuarioEditarDto.mac
            ) {
              usuarioEditarDto.location = observacion.location;
              console.log('ubicacion',usuarioEditarDto);
              this.actualizarUsuario = this._usuarioModel.findByIdAndUpdate(
                usuarioId,
                usuarioEditarDto,
                { new: true },
              );
            } else {
              this.actualizarUsuario = this._usuarioModel.findByIdAndUpdate(
                usuarioId,
                usuarioEditarDto,
                { new: true },
              );
            }
          });
        }else{
          console.error('No existen registros de escaneo');
          throw new BadRequestException('No existen registros de escaneo');
        }
      },
      error => {
        console.error('No existen registros de escaneo', error);
      },
    );
    return await this.actualizarUsuario;
  }

  // eliminar un usuario
  async eliminarUsuario(usuarioId: string): Promise<any> {
    const eliminarUsuario = await this._usuarioModel.findByIdAndDelete(
      usuarioId,
    );
    return eliminarUsuario;
  }

  async findEmail(correo: string) {
    return this._usuarioModel.findOne(correo);
  }

  async resetearPassword(id: number) {
    const password = randomize('Aa0', 8);
    return await this._usuarioModel.update(id, { password });
  }
}
