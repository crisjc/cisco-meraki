import { Document } from 'mongoose';
import { EventoInterface } from 'evento/evento.interface';
import { LocationInterface } from 'scaneo/body.interface';

export interface NotificacionInterface extends Document {
  mensaje: string;
  fecha: Date;
  isleido: boolean;
}
export interface UsuarioInterface extends Document {
  nombre: string;
  correo: string;
  password: string;
  rol: string;
  ip?: string;
  mac?: string;
  eventos?: Array<EventoInterface>;
  notificaciones?: Array<NotificacionInterface>;
  location?: LocationInterface;
}
