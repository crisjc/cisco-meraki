import { Document } from 'mongoose';
import { LocationInterface } from 'scaneo/body.interface';

export interface EventoInterface extends Document {
  nombre: string;
  descripcion: string;
  direccion: string;
  horaInicio: string;
  imagen: string;
  enlace: string;
  location: LocationInterface;
}

