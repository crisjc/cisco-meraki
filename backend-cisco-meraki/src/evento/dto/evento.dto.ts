import { IsNotEmpty, IsString } from 'class-validator';
import { LocationInterface } from 'scaneo/body.interface';
export class EventoDto {
  @IsString()
  @IsNotEmpty()
  nombre: string;

  @IsString()
  @IsNotEmpty()
  descripcion: string;

  @IsString()
  @IsNotEmpty()
  direccion: string;

  @IsString()
  @IsNotEmpty()
  horaInicio: string;

  @IsString()
  imagen?: string;

  @IsString()
  enlace?: string;
  
  @IsNotEmpty()
  location: LocationInterface;
}
