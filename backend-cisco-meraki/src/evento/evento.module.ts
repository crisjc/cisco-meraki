import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { eventoSchema } from './evento.schema';
import { EventoService } from './evento.service';
import { EventoController } from './evento.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Evento', schema: eventoSchema }]),
  ],
  controllers: [EventoController],
  providers: [EventoService],
  exports: [EventoService]
})
export class EventoModule {}
