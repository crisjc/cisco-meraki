import * as mongoose from 'mongoose';
import { locationSchema } from 'scaneo/scaneo.shema';

export const eventoSchema = new mongoose.Schema({
  nombre: String,
  descripcion: String,
  direccion: String,
  horaInicio: String,
  horaFin: String,
  imagen: String,
  enlace: String,
  location: locationSchema,
});
