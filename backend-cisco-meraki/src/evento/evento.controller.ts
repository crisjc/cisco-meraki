import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Param,
  NotFoundException,
  Post,
  Body,
  Put,
  Query,
  UseInterceptors,
  Delete,
} from '@nestjs/common';
import { EventoService } from './evento.service';
import { EventoDto } from './dto/evento.dto';

@Controller('evento')
export class EventoController {
  constructor(private readonly _eventoService: EventoService) {}

  // metodo obtener todos
  @Get('/')
  async obtenerTodosEventos(@Res() res) {
    const eventos = await this._eventoService.obtenerEventos();
    return res.status(HttpStatus.OK).json(eventos);
  }

  // metodo obtener uno
  @Get('/:eventoId')
  async obtenerEvento(@Res() res, @Param('eventoId') eventoId) {
    const evento = await this._eventoService.obtenerEvento(eventoId);
    if (!evento) throw new NotFoundException('Evento no existe');
    return res.status(HttpStatus.OK).json(evento);
  }

  //crear un evento
  @Post('/crear')
  async crearEvento(@Res() res, @Body() crearEvento: EventoDto) {
    const evento = await this._eventoService.crearEvento(crearEvento);
    return res.status(HttpStatus.OK).json({
      message: 'Evento creado',
      evento,
    });
  }

  // actualizar evento  /actualizar?eventoId=5c9d45e705ea4843c8d0e8f7
  @Put('/actualizar')
  async actualizarEvento(
    @Res() res,
    @Body() eventoDto: EventoDto,
    @Query('eventoId') eventoId,
  ) {
    const actualizarEvento = await this._eventoService.actulizarEvento(
      eventoId,
      eventoDto,
    );
    if (!actualizarEvento) throw new NotFoundException('Este evento no existe');
    return res.status(HttpStatus.OK).json({
      message: 'Evento actulizado correctamente',
      actualizarEvento,
    });
  }

  // eliminar el evento /eliminar?eventoId=5c9d45e705ea4843c8d0e8f7
  @Delete('/eliminar')
  async eliminarEvento(@Res() res, @Query('eventoId') eventoId) {
    const eliminarEvento = await this._eventoService.eliminarEvento(eventoId);
    if (!eliminarEvento) throw new NotFoundException('Evento no existe!');
    return res.status(HttpStatus.OK).json({
      message: 'Evento eliminado correctamente',
      eliminarEvento,
    });
  }
}
