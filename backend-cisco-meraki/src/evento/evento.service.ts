import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EventoInterface } from './evento.interface';
import { EventoDto } from './dto/evento.dto';

@Injectable()
export class EventoService {
  constructor(
    @InjectModel('Evento')
    private readonly _eventoModel: Model<EventoInterface>,
  ) {}

  //obtener todos los eventos
  async obtenerEventos(): Promise<EventoInterface[]> {
    const eventos = await this._eventoModel.find();
    return eventos;
  }

  // Obtener un evento
  async obtenerEvento(eventoId: string): Promise<EventoInterface> {
    const evento = await this._eventoModel.findById(eventoId);
    return evento;
  }

  // crear un evento
  async crearEvento(crearEventoDto: EventoDto): Promise<EventoInterface> {
    const nuevoEvento = new this._eventoModel(crearEventoDto);
    return nuevoEvento.save();
  }

  // actulizar un evento
  async actulizarEvento(
    eventoId: string,
    crearEventoDto: EventoDto,
  ): Promise<EventoInterface> {
    const actualizarEvento = await this._eventoModel.findByIdAndUpdate(
      eventoId,
      crearEventoDto,
      { new: true },
    );
    return actualizarEvento;
  }

  // eliminar un evento
  async eliminarEvento(eventoId: string): Promise<any> {
    const eliminarEvento = await this._eventoModel.findByIdAndDelete(eventoId);
    return eliminarEvento;
  }
}
