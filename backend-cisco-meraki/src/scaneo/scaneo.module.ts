import { Module } from '@nestjs/common';
import { ScaneoController } from './scaneo.controller';
import { ScaneoService } from './scaneo.service';
import { MongooseModule } from '@nestjs/mongoose';
import { scaneoSchema } from './scaneo.shema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Scaneo', schema: scaneoSchema }]),
  ],
  controllers: [ScaneoController],
  providers: [ScaneoService],
  exports: [ScaneoService],
})
export class ScaneoModule {}
