import { Document } from 'mongoose';

export interface BodyInterface extends Document {
  version: string;
  secret: string;
  type: string;
  data: DataInterface;
}

export interface DataInterface extends Document {
  apMac: string;
  apTags: Array<string>;
  apFloors: Array<string>;
  observations: Array<ObservationInterface>;
}

export interface ObservationInterface extends Document {
  clientMac: string;
  ipv4: string;
  ipv6: string;
  seenTime: string;
  seenEpoch: number;
  ssid: string;
  rssi: number;
  manufacturer: string;
  os: string;
  location: LocationInterface;
}

export interface LocationInterface extends Document {
  lat: number;
  lng: number;
  unc: number;
  x: Array<number>;
  y: Array<number>;
}
