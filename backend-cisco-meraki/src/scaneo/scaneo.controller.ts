import { Controller, Get, Res, HttpStatus, Post, Body } from '@nestjs/common';
import { ScaneoService } from './scaneo.service';
import { BodyInterface } from './body.interface';

@Controller('scaneo')
export class ScaneoController {
  constructor(private readonly _servicios: ScaneoService) {}

  @Get()
  async enviarValidator(@Res() res) {
    console.log('esto se envio con response', this._servicios.validador);
    return await res.status(HttpStatus.OK).send(this._servicios.validador);
  }

  @Post()
  async verificarSecreto(@Res() res, @Body() bodyData: BodyInterface) {
    const scaneo = await  this._servicios.validarSecreto(bodyData);
    //const josnbody = JSON.parse(JSON.stringify(bodyData));
    return await res.status(HttpStatus.OK).json({
        message: 'Escaneo Guardado',
        scaneo,
      });
  }
}
