import { Injectable } from '@nestjs/common';
import { BodyInterface } from './body.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from } from 'rxjs';
@Injectable()
export class ScaneoService {
  secret: string = process.env.SECRET || 'testing123';
  validador: string =
    process.env.VALIDATOR || 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
  route = process.env.ROUTE || '/scaneo';
  ultimoEscaneo: BodyInterface;
  constructor(
    @InjectModel('Scaneo')
    private readonly _scaneoModel: Model<BodyInterface>,
  ) {}

  enviarGetValidator() {
    return this.validador;
  }
  // Este servicio me permite agregar un scaneo si se cumple el secreto
  async validarSecreto(bodyData: BodyInterface) {
    if (bodyData.secret === this.secret) {
      console.log('Secreto Valido');
      const crearScaneo = new this._scaneoModel(bodyData);
      return await crearScaneo.save();
    } else {
      console.log('Secreto invalido');
      return await 0;
    }
  }

  //obtener todos los scaneos
  async obtenerUltimoEscaneo(): Promise<BodyInterface> {
    const scaneos = await this._scaneoModel.find();
    return scaneos[scaneos.length - 1];
  }
}
