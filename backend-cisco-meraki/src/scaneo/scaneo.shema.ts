import * as mongoose from 'mongoose';

export const locationSchema = new mongoose.Schema({
  lat: Number,
  lng: Number,
  unc: Number,
  x: [Number],
  y: [Number],
});

export const observationSchema = new mongoose.Schema({
  clientMac: String,
  ipv4: String,
  ipv6: String,
  seenTime: String,
  seenEpoch: Number,
  ssid: String,
  rssi: Number,
  manufacturer: String,
  os: String,
  location: locationSchema,
});

export const dataSchema = new mongoose.Schema({
  apMac: String,
  apTags: [String],
  apFloors: [String],
  observations: [observationSchema],
});
export const scaneoSchema = new mongoose.Schema({
  version: String,
  secret: String,
  type: String,
  data: dataSchema,
});
