import { IsNotEmpty } from 'class-validator';
import { DataInterface } from 'scaneo/body.interface';
export class ScaneoDto {
  version: string;

  secret: string;

  type: string;

  data: DataInterface;
}
