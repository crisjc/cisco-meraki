import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScaneoModule } from './scaneo/scaneo.module';
import { env } from './environments/environment';
import { MongooseModule } from '@nestjs/mongoose';
import { EventoModule } from './evento/evento.module';
import { UsuarioModule } from './usuario/usuario-module';

@Module({
  imports: [
    ScaneoModule,
    MongooseModule.forRoot(env.url, { useNewUrlParser: true, useFindAndModify: false  }),
    EventoModule,
    UsuarioModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
