# Documentacion de la aplicacion 

## Índice
1. [ Caso de uso o historias de usuario](#id1)
2. [Pre requisitos y herramientas a utilizarse](#id2)
3. [API cisco meraki](#id3)
4. [Instalación y Configuración](#id4)
5. [Desarrollo de la APP movil](#id5)


<a name="id1"></a>
## Caso de uso o historias de usuario
#### TEMA  : Movilidad Campus EPN
* Yo como comunidad perteneciente o no a la escuela politécnica nacional quiero una aplicación que me permita en caso de no conocer el campos me pueda mostrar la ruta que yo puedo tomar para poder dirigirme a algún lugar dentro del campus o así mismo a un evento dentro. 

**Primer alcance**
USUARIO
Objetivo: Aplicacicion para eventos dentro de la comunidad politecnica 
* Yo como usuario quiero que me envien una notificacion cuando yo pase por el lugar del evento
* Yo como usuario necestio saber el nombr del evento y la direccion hora y fecha del evento gratuidad.
* Yo quiero usuario quiero ver el historial de la notificaciones en el celular con una opcion en cada notificion que me diga como ir hasta el evento
ADMIN
* Yo como administrador voy a poder crear los eventos
* Yo como admistrador voy a ver el total de asistente dentro del evento
* Yo como admistrador voy poder la calificacion de los usuario
* Yo como admistrador voy a ver el total diario  de usuario 
* Yo como usuario voy a pode ve tablas estaditicas de la interaccion de los usuarios
**Segundo Alcance**
* Yo como usuario necesito saber si al servicio que voy a acceder se encuentra disponible.
* Yo como usuario necesito recibir notificaciones de los eventos a pasar por el lugar del evento. 
* Yo como administrador necesito un reporte de las personas conectadas por día.
* Yo como administrador necesito ver en que horas son horas pico en los diferente servicios que ofrece la universidad bibliotecas, secretarias, estacionamientos, espacios verdes, bares. 

#### Criterios de aceptación 

* Políticas de seguridad recomendado en la api 
* Cada cierto tiempo si no se completa cancelar  y no enviar ningún tipo de información.


<a name="id2"></a>
## Pre requisitos y herramientas a utilizarse

*	Backend en nest si no express de node
*	Frontend aplicacion en ionic 
*	Base de datos no relacional
*	Servicio cloud para el servidor en AWS
*	Docker (para pruebas)
*	Api de cisco meraki
*	Datos de prueba json simulaciones de post y put 
*	Diseño de interfaces en invision o figma
*	Board para scrum 

<a name="id3"></a>
## API cisco meraki

> Aqui se detallara  la teoria de la api mas en especifico su funcionamiento

#### API of Scanning 
![]([https://files.mtstatic.com/site_5814/2614/0?Expires=1557638514&Signature=aq3hx3N3s~DR7RH5zR6JhWSJUxZOwNnPfJkWnroUKyusbT1gv8YG-QYCIZcnAHnVYefLUYmP~odvhQCYFflMUPSF6J~XLrEcLNMFsba-oc0pFnkg418HTkKySNQhYKaWqmWfa3zSBiI1w4aDmHJLZyJNI9NPHGGyQgnGXMEK3J0_&Key-Pair-Id=APKAJ5Y6AV4GI7A555NA](https://files.mtstatic.com/site_5814/2614/0?Expires=1557638514&Signature=aq3hx3N3s~DR7RH5zR6JhWSJUxZOwNnPfJkWnroUKyusbT1gv8YG-QYCIZcnAHnVYefLUYmP~odvhQCYFflMUPSF6J~XLrEcLNMFsba-oc0pFnkg418HTkKySNQhYKaWqmWfa3zSBiI1w4aDmHJLZyJNI9NPHGGyQgnGXMEK3J0_&Key-Pair-Id=APKAJ5Y6AV4GI7A555NA))
Arquitectura de comunicacion 

* Los datos son receptados por el ap desde cualquier punto cada minuto son enviador al servidor de la empresa como post y en formato json.
* Dispositivos de wifi asociados o no y de bluetooth de baja energia 
* Apartir de la ubicacion del AP este estima la posible ubicacion del cliente

**Formato del post enviado desde nube MERAKI**

    { 
    "versión": "2.0",
     "secreto": <cadena>, 
     "tipo": <tipo de evento>, 
     "datos": <datos específicos del evento> 
     }
**La parte de la data con el siguiente formato**
~~~
{ 
	"apMac": <cadena>, 
	"apTags": [<string, ...],
	 "apFloors": [<string>, ...], 
	 "observaciones": [ 
		 { "clientMac": <string>, 
		 "ipv4": <string>, 
		 "ipv6": <string>, 
		 "seenTime": <string>, 
		 "seenEpoch": <integer>,
		  "ssid": <string>, 
		  "rssi": <integer>, 
		  "fabricante ": <string>,
		   " os ": <string>, 
		   " location ": 
			   { " lat ": <decimal>, 
			    " lng ": <decimal>,
			     "unc": <decimal>, 
			     "x": [<decimal>, ...], 
			     "y": [<decimal>, ...]
			     },
			 }, ...
	 ] 
 }
~~~

### Habilitar API de escaneo
La API de escaneo se configura en el Panel de Meraki en la página de **Red amplia>** Configuración **general** en unos pocos pasos simples:

1.  Configure y hospede su servidor HTTP para recibir objetos JSON.
    -   [Ejemplo de receptor de la versión 1](https://documentation.meraki.com/@api/deki/files/2604/merakiReceiver.js.zip?revision=1 "merakiReceiver.js.zip")
    -   Ejemplo de receptor de la versión 2 ( [https://github.com/meraki/cmx-api-app](https://github.com/meraki/cmx-api-app "https://github.com/meraki/cmx-api-app") )
2.  Active la API seleccionando API de escaneo habilitada en el cuadro desplegable.
    
3.  Especifique una URL de publicación y el secreto de autenticación (el servidor HTTP utiliza el secreto para validar que las publicaciones JSON provienen de la nube de Meraki)
4.  Especifique qué versión de API de escaneo está preparado para recibir y procesar su servidor HTTP.
5.  Haga clic en el botón **Validar** y, para una validación exitosa, haga **clic en el** botón **Guardar** para guardar la página.
6.  Tras la primera conexión, la nube Meraki realizará un solo GET HTTP; el servidor debe devolver la cadena de validador específica de la organización como respuesta, que verificará la identidad de la organización como el cliente de Cisco Meraki. La nube Meraki comenzará a realizar publicaciones JSON.

 Para la parte de la seguridad se debera revisar la parte de 	OPT OUT
 [link]([https://account.meraki.com/optout](https://account.meraki.com/optout))
 * Lo mismo se aplica para la busqueda por bluetooth
<a name="id4"></a>
## Instalacion y configuracionf

las cabeceras que se mandan son las siguientes:

cisco prueba 

    X-Cisco-Meraki-API-Key : 6bec40cf957de430a6f1f2baa056b99a4fac9ea0

cisco

    X-Cisco-Meraki-API-Key : b1ddd9180f6e083262a5bf5c43589fb534e78ac3

cristhian

    X-Cisco-Meraki-API-Key : a5eeb14cdee0a67c4d9ce59c067cd5a24dea3438 
    
    
    Content-Type : application/json

para saber el request de la organización 

    https://api.meraki.com/api/v0/organizations/549236/apiRequests
pasa saber las organizaciones 

    https://api.meraki.com/api/v0/organizations/
para saber la organización 

    https://api.meraki.com/api/v0/organizations/549236/apiRequests


<a name="id5"></a>

## Desarrollo de la app movil

> Aqui se pondran todos los metodos http a utlizarce y la estructura de su formato json. 

[poastman localizacion](https://documenter.getpostman.com/view/897512/71FUpux)

[Api generales dashboard](https://dashboard.meraki.com/api_docs#alert-settings)

[dashboard](https://n149.meraki.com/DNSMB5-tim.geesj/n/w8dMravc/manage/configure/general)

[Api rest](https://developer.cisco.com/meraki/api/#/rest/guides/rest-api-quick-start)

[team webex](https://teams.webex.com/spaces/576b74c0-7052-11e9-b0f7-170adcc28b8b/chat)

[Comunidad](https://community.meraki.com/t5/Solutions-APIs/bd-p/api)

[Code Exchage](https://developer.cisco.com/codeexchange/github/repo/IndoorLocation/meraki-indoor-location)
